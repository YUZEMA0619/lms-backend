﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.IO;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using LMSbackend.Interface;
using LMSbackend.DTO;
using LMSbackend.Model;

namespace LMSbackend.Controllers
{
    [Authorize]
    [Route("api/Users")]
    public class UserController : Controller
    {
        private IUserDataStore _userDataStore;

        public UserController(IUserDataStore userDataStore)
        {
            _userDataStore = userDataStore;
        }

        /*--------------------------------USER-----------------------------------*/

        [Authorize(Roles = "Admin")]
        [HttpGet("admin")]
        public IActionResult AdminGet()
        {
            var result = _userDataStore.GetAllUsers();
            return Ok(result);
        }

        [HttpDelete("DeleteStudentUser/{userId}")]
        public IActionResult DeleteStudentUser(int userId)
        {
            var result = _userDataStore.DeleteStudentUser(userId);

            return Ok(result);
        }



        [HttpDelete("DeleteLecturerUser/{userId}")]
        public IActionResult DeleteLecturerUser(int userId)
        {
            var result = _userDataStore.DeleteLecturerUser(userId);

            return Ok(result);
        }

        [AllowAnonymous]
        [HttpPost("LogIn")]
        public IActionResult LogIn([FromBody] LoginDto loginDto)
        {
            string account = loginDto.Account.ToLower();
            string password = loginDto.Password.ToLower();
            string verificationNumber = loginDto.VerificationNumber;
            var result = _userDataStore.LogIn(account, password, verificationNumber);

            return Ok(result);
        }

        [HttpGet]
        public IActionResult GetUserInfo()
        {
            string stringId = this.User.Identity.Name;
            int id = Convert.ToInt32(stringId);
            UserInfoDto result = _userDataStore.GetUserInfo(id);
            return Ok(result);
        }

        [AllowAnonymous]
        [HttpPost("SignUp")]
        public IActionResult SignUp([FromBody] SignUpDto signUpDto)
        {
            UserDetail userDetail = new UserDetail() { UserType = signUpDto.UserType, Account = signUpDto.Account, Phone = signUpDto.Phone, Password = signUpDto.Password, Name = signUpDto.Name };
            var result = _userDataStore.SignUp(userDetail);

            return Ok(result);
        }


        [HttpPost("UploadFiles")]
        public IActionResult UploadFiles(IFormFile file)
        {
            string stringId = this.User.Identity.Name;
            int id = Convert.ToInt32(stringId);
            var result = _userDataStore.UploadIcon(id, file);
            return Ok(result);
        }

        [AllowAnonymous]
        [HttpGet("GetUserIcon/{UserId}")]
        public IActionResult GetFiles(int UserId)
        {
            UserDetail user = _userDataStore.GetIcon(UserId);
            string path = user.IconPath;
            if (System.IO.File.Exists(path))
            {
                string contentType = user.ContentType;
                Byte[] b = System.IO.File.ReadAllBytes(@path);
                return File(b, contentType);
            }
            else
            {
                //string defaultPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/images", "user0icon.png");
                string defaultPath = Path.Combine("/home/bitnami/lms-backend/LMSbackend/LMSbackend/wwwroot/images", "user0icon.png");
                Byte[] b = System.IO.File.ReadAllBytes(@defaultPath);
                return File(b, "image/png");
            }
        }

        [AllowAnonymous]
        [HttpGet("VerifyEmail/{id}/{verifyCode}")]
        public IActionResult VerifyEmail(int id, string verifyCode)
        {
            var result = _userDataStore.VerifyEmail(id, verifyCode);
            return Ok(result);
        }

        [HttpPost("UpdateEmail")]
        public IActionResult UpdateEmailAddress([FromBody] UserInfoDto userInfoDto)
        {
            string stringId = this.User.Identity.Name;
            int id = Convert.ToInt32(stringId);
            string result = _userDataStore.UpdateEmailAddress(id, userInfoDto.Email);
            return Ok(result);
        }


        /*--------------------------------USER (End)-----------------------------*/

        /*-----------------------------STUDENT-----------------------------------*/

        [HttpPost("enrollStudent")]
        public IActionResult Post([FromBody] Student value)
        {
            var result = _userDataStore.AddStudent(value);
            return Ok(result);
        }


        [HttpGet("allStudents")]
        public IActionResult GetAllStudents()
        {
            var result = _userDataStore.GetAllStudents();
            return Ok(result);
        }


        [HttpPost("studentcourse/{studentid}/{courseid}")]
        public IActionResult AddStudentToCourse(int studentid, int courseid)
        {
            var result = _userDataStore.AddStudentToCourse(studentid, courseid);
            return result != null ? Ok(result) : (IActionResult)NotFound();
        }


        [HttpDelete("studentcourse/{studentid}/{courseid}")]
        public IActionResult DeleteStudentFromCourse(int studentid, int courseid)
        {
            var result = _userDataStore.DeleteStudentFromCourse(studentid, courseid);
            return result != null ? Ok(result) : (IActionResult)NotFound();
        }


        [HttpDelete("DeleteStudent/{studentId}")]
        public IActionResult DeleteStudent(int studentId)
        {
            var result = _userDataStore.DeleteStudent(studentId);

            return Ok(result);
        }


        /*--------------------------STUDENT (End)--------------------------------*/
        /*----------------------------LECTURER-----------------------------------*/

        [HttpPost("enrollLecturer")]
        public IActionResult AddLecturer([FromBody] Lecturer value)
        {
            var result = _userDataStore.AddLecturer(value);
            return Ok(result);
        }


        [HttpGet("allLecturers")]
        public IActionResult GetAllLecturers()
        {
            var result = _userDataStore.GetAllLecturers();
            return Ok(result);
        }

        [HttpPost("lecturercourse/{lecturerid}/{courseid}")]
        public IActionResult AddLecturerToCourse(int lecturerid, int courseid)
        {
            var result = _userDataStore.AddLecturerToCourse(lecturerid, courseid);
            return result != null ? Ok(result) : (IActionResult)NotFound();
        }


        [HttpDelete("lecturercourse/{lecturerid}/{courseid}")]
        public IActionResult DeleteLecturerFromCourse(int lecturerid, int courseid)
        {
            var result = _userDataStore.DeleteLecturerFromCourse(lecturerid, courseid);
            return result != null ? Ok(result) : (IActionResult)NotFound();
        }

        [HttpDelete("DeleteLecturer/{lecturerId}")]
        public IActionResult DeleteLecturer(int lecturerId)
        {
            var result = _userDataStore.DeleteLecturer(lecturerId);

            return Ok(result);
        }

        /*--------------------------LECTURER (End)-------------------------------*/
        /*------------------------------COURSE-----------------------------------*/

        [HttpGet("CourseDetail/{courseId}")]
        public IActionResult GetCourseDetail(int courseId)
        {
            var result = _userDataStore.GetCourseDetail(courseId);
            return Ok(result);
        }


        [HttpGet("StudentsForCourse/{courseId}")]
        public IActionResult GetAllStudentsForCourse(int courseId)
        {
            var result = _userDataStore.GetAllStudentsForCourse(courseId);
            return Ok(result);
        }


        [HttpGet("allCourses")]
        public IActionResult GetAllCourses()
        {
            var result = _userDataStore.GetAllCourses();
            return Ok(result);
        }

        [HttpGet("LecturersForCourse/{courseId}")]
        public IActionResult GetAllLecturersForCourse(int courseId)
        {
            var result = _userDataStore.GetAllLecturersForCourse(courseId);
            return Ok(result);
        }

        [HttpGet("CoursesForLecturer/{lecturerId}")]
        public IActionResult GetAllCoursesForLecturer(int lecturerId)
        {
            var result = _userDataStore.GetAllCoursesForLecturer(lecturerId);
            return Ok(result);
        }


        [HttpPost("AddCourse")]
        public IActionResult AddCourse([FromBody] Course value)
        {
            ReturnDto result = _userDataStore.AddCourse(value);
            return Ok(result);
        }

        [HttpPut("UpdateCourse/{courseId}")]
        public IActionResult UpdateCourse([FromBody] Course value, int courseId)
        {
            try
            {
                ReturnDto result = _userDataStore.UpdateCourse(value, courseId);
                return Ok(result);
            }
            catch (DbUpdateConcurrencyException ex)
            {
                string message = null;
                var entityPropertyValues = ex.Entries.Single().GetDatabaseValues();
                if (entityPropertyValues == null)
                {
                    message = "This course has been deleted by another user!";
                }
                else
                {
                    message = "This course has been updated by another user! Click Save Button Again To Contiune  or  Click Cancel Button To Cancel .";

                }
                return Ok(new ReturnDto { Status = 0, Message = message });
            }

        }

        [HttpDelete("DeleteCourse/{courseId}")]
        public IActionResult DeleteCourse(int courseId)
        {
            var result = _userDataStore.DeleteCourse(courseId);

            return Ok(result);
        }
        /*--------------------------COURSE (End)---------------------------------*/

    
    }
}

