﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json.Serialization;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System.Text;
using LMSbackend.Interface;
using LMSbackend.Repository;
using LMSbackend.Model;

namespace LMSbackend
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddMvc()
                .AddJsonOptions(o =>
                {
                    if (o.SerializerSettings.ContractResolver != null)
                    {
                        var castedResolver = o.SerializerSettings.ContractResolver
                                        as DefaultContractResolver;
                        castedResolver.NamingStrategy = null;
                    }
                    //ignore loop
                    o.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                    o.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
                    o.SerializerSettings.MissingMemberHandling = Newtonsoft.Json.MissingMemberHandling.Ignore;
                });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "My API", Version = "v1" });
                c.AddSecurityDefinition("Bearer", new ApiKeyScheme { In = "header", Description = "This is the JWT for Auth", Name = "Authorization", Type = "apiKey" });
                c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>> {
                 { "Bearer", Enumerable.Empty<string>() },
            });
            });

            services.Configure<ForwardedHeadersOptions>(options =>
            {
                options.ForwardedHeaders =
                    ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto;
            });

            var key = Encoding.ASCII.GetBytes("ThisissercertKeyforFullStackWebAPIDEMO");

            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });

            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();
                    });
            });

            services.AddDbContext<SchoolDBContext>();
            services.AddScoped<IUserDataStore, UserDataStore>();


        }

        public static void Seed(IApplicationBuilder applicationBuilder)
        {
            using (var serviceScope = applicationBuilder.ApplicationServices.GetRequiredService<IServiceScopeFactory>()
                    .CreateScope())
            {
                SchoolDBContext context = serviceScope.ServiceProvider.GetService<SchoolDBContext>();
                //data seeding
                if (!context.Users.Any())
                {
                    context.Users.Add(new UserDetail()
                    {
                        Account = "student001",
                        Name = "Leo",
                        Phone = "0451070619",
                        Phone_Activated = true,
                        UserType = "Student",
                        Password = "bOH0t5VVY8kNfmifEMLM6mxqddpYUseOuDd5olJIM8U=",
                        Salt = "9Nob+7Ntysm5EMv2a8MD5XoFAl6QfUHGKV9tNe1ltsyI2BwQ"
                    });
                    context.Users.Add(new UserDetail()
                    {
                        Account = "student002",
                        Name = "Alex",
                        Phone = "0451070619",
                        Phone_Activated = true,
                        UserType = "Student",
                        Password = "bOH0t5VVY8kNfmifEMLM6mxqddpYUseOuDd5olJIM8U=",
                        Salt = "9Nob+7Ntysm5EMv2a8MD5XoFAl6QfUHGKV9tNe1ltsyI2BwQ"
                    });
                    context.Users.Add(new UserDetail()
                    {
                        Account = "student003",
                        Name = "Jerry",
                        Phone = "0451070619",
                        Phone_Activated = true,
                        UserType = "Student",
                        Password = "bOH0t5VVY8kNfmifEMLM6mxqddpYUseOuDd5olJIM8U=",
                        Salt = "9Nob+7Ntysm5EMv2a8MD5XoFAl6QfUHGKV9tNe1ltsyI2BwQ"
                    });
                    context.Users.Add(new UserDetail()
                    {
                        Account = "lecturer001",
                        Name = "Heather",
                        Phone = "0451070619",
                        Phone_Activated = true,
                        UserType = "Lecturer",
                        Password = "bOH0t5VVY8kNfmifEMLM6mxqddpYUseOuDd5olJIM8U=",
                        Salt = "9Nob+7Ntysm5EMv2a8MD5XoFAl6QfUHGKV9tNe1ltsyI2BwQ"
                    });
                    context.Users.Add(new UserDetail()
                    {
                        Account = "lecturer002",
                        Name = "Tom",
                        Phone = "0451070619",
                        Phone_Activated = true,
                        UserType = "Lecturer",
                        Password = "bOH0t5VVY8kNfmifEMLM6mxqddpYUseOuDd5olJIM8U=",
                        Salt = "9Nob+7Ntysm5EMv2a8MD5XoFAl6QfUHGKV9tNe1ltsyI2BwQ"
                    });
                    context.Users.Add(new UserDetail()
                    {
                        Account = "lecturer003",
                        Name = "Aaron",
                        Phone = "0451070619",
                        Phone_Activated = true,
                        UserType = "Lecturer",
                        Password = "bOH0t5VVY8kNfmifEMLM6mxqddpYUseOuDd5olJIM8U=",
                        Salt = "9Nob+7Ntysm5EMv2a8MD5XoFAl6QfUHGKV9tNe1ltsyI2BwQ"
                    });
                    context.Users.Add(new UserDetail()
                    {
                        Account = "admin001",
                        Name = "Tony",
                        Phone = "0451070619",
                        Phone_Activated = true,
                        UserType = "Admin",
                        Password = "bOH0t5VVY8kNfmifEMLM6mxqddpYUseOuDd5olJIM8U=",
                        Salt = "9Nob+7Ntysm5EMv2a8MD5XoFAl6QfUHGKV9tNe1ltsyI2BwQ"
                    });
                    context.Users.Add(new UserDetail()
                    {
                        Account = "admin002",
                        Name = "Jimmy",
                        Phone = "0451070619",
                        Phone_Activated = true,
                        UserType = "Admin",
                        Password = "bOH0t5VVY8kNfmifEMLM6mxqddpYUseOuDd5olJIM8U=",
                        Salt = "9Nob+7Ntysm5EMv2a8MD5XoFAl6QfUHGKV9tNe1ltsyI2BwQ"
                    });
                    context.Users.Add(new UserDetail()
                    {
                        Account = "admin003",
                        Name = "Edward",
                        Phone = "0451070619",
                        Phone_Activated = true,
                        UserType = "Admin",
                        Password = "bOH0t5VVY8kNfmifEMLM6mxqddpYUseOuDd5olJIM8U=",
                        Salt = "9Nob+7Ntysm5EMv2a8MD5XoFAl6QfUHGKV9tNe1ltsyI2BwQ"
                    });
                }

                context.SaveChanges();
            }
        }


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            Seed(app);
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            //else
            //{
            //    app.UseHsts();
            //}

            //app.UseHttpsRedirection();


            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });

            app.UseAuthentication();
            app.UseCors("AllowAll");
            app.UseStaticFiles();
            app.UseMvc();
        }
    }
}

