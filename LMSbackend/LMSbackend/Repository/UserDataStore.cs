﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using LMSbackend.DTO;
using LMSbackend.Interface;
using LMSbackend.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace LMSbackend.Repository
{
    public class UserDataStore : IUserDataStore
    {
        private SchoolDBContext _schoolDBContext;

        public UserDataStore(SchoolDBContext schoolDBContext)
        {
            _schoolDBContext = schoolDBContext;
        }


        /*--------------------------------USER-----------------------------------*/
        public ReturnDto LogIn(string account, string password, string verificationNumber)
        {
            var users = _schoolDBContext.Users.ToList();
            int selectedUserId = -1;
            foreach (var user in users)
            {
                if (user.Account == account)
                {
                    selectedUserId = user.Id;
                }
            }
            if (selectedUserId == -1)
            {
                return new ReturnDto() { Status = -1, Message = "Account not exist" };
            }
            UserDetail selectedUser = _schoolDBContext.Users.Find(selectedUserId);
            var salt = selectedUser.Salt;
            var inputPassword = PSWEncryption(password, salt);
            if (selectedUser == null || selectedUser.Password != inputPassword)
            {
                return new ReturnDto() { Status = -1, Message = "Incorrect Username or Password." };
            }
            else if (!selectedUser.Phone_Activated)
            {
                DateTime timeNow = DateTime.Now;
                if (timeNow > selectedUser.VNExpiredDate)
                {
                    _schoolDBContext.Users.Remove(selectedUser);
                    _schoolDBContext.SaveChanges();
                    return new ReturnDto() { Status = -2, Message = "Expired, Please Sign Up Again!" };
                }

                if (verificationNumber == selectedUser.VerificationNumber)
                {
                    selectedUser.Phone_Activated = true;
                    selectedUser.VerificationNumber = null;
                    _schoolDBContext.SaveChanges();
                    return new ReturnDto() { Status = 0, Message = "Activation Successful!" };
                }
                else
                {
                    return new ReturnDto() { Status = -3, Message = "wrong verification number" };
                }
            }
            else
            {
                //generate token
                var user = _schoolDBContext.Users.Find(selectedUserId);
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes("ThisissercertKeyforFullStackWebAPIDEMO");
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                     {
                         //new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                         //new Claim(ClaimTypes.Name, user.Name),
                         new Claim(ClaimTypes.Name, user.Id.ToString()),
                         new Claim(ClaimTypes.Role, user.UserType)
                     }),
                    Expires = DateTime.UtcNow.AddHours(1.0),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                };
                var token = tokenHandler.CreateToken(tokenDescriptor);
                string stringToken = tokenHandler.WriteToken(token);
                return new ReturnDto() { Status = 1, Message = stringToken };
            }
        }

        public ReturnDto SignUp(UserDetail userDetail)
        {
            var users = _schoolDBContext.Users.ToList();
            foreach (var user in users)
            {
                if (user.Account == userDetail.Account.ToLower())
                {
                    return new ReturnDto() { Status = 0, Message = "Error!UserId is existed." };
                }
            }

            if (userDetail.Account.Length < 8 || userDetail.Password.Length < 8)
            {
                return new ReturnDto() { Status = -1, Message = "Error! Makesure userId and password contain at least 8 characters. Can't start with 0" };
            }
            else if (userDetail.Phone.Length != 10)
            {
                return new ReturnDto() { Status = -2, Message = "Error! Please enter correct phone number." };
            }



            string verificationNumber = VerificationNumberGenerator.Generate();
            DateTime expiredDateTime = DateTime.Now.AddMinutes(10.0);
            bool result = SendMessage(userDetail.Phone, verificationNumber, expiredDateTime);
            if (result == false)
            {
                return new ReturnDto() { Status = -3, Message = "Error! while sending message! " };
            }
            userDetail.Id = 0;
            userDetail.Account = userDetail.Account.ToLower();

            string salt = GetSalt();
            userDetail.Salt = salt;
            userDetail.Password = PSWEncryption(userDetail.Password.ToLower(), salt);

            userDetail.Phone_Activated = false;
            userDetail.VerificationNumber = verificationNumber;
            userDetail.VNExpiredDate = expiredDateTime;
            bool IsAdmin = userDetail.UserType == "Admin" ? true : false;
            userDetail.Enrolled_Activated = IsAdmin;
            _schoolDBContext.Users.Add(userDetail);
            _schoolDBContext.SaveChanges();
            return new ReturnDto() { Status = 1, Message = "Success! Please activate your account by Verification Number" };

        }

        public IEnumerable<Student> DeleteStudentUser(int userId)
        {
            UserDetail selectedUser = _schoolDBContext.Users.Find(userId);
            _schoolDBContext.Users.Remove(selectedUser);
            _schoolDBContext.SaveChanges();
            return _schoolDBContext.Students
                                   .Include(s => s.UserDetail)
                                   .ToList();
        }

        public IEnumerable<Lecturer> DeleteLecturerUser(int userId)
        {
            UserDetail selectedUser = _schoolDBContext.Users.Find(userId);
            _schoolDBContext.Users.Remove(selectedUser);
            _schoolDBContext.SaveChanges();
            return _schoolDBContext.Lecturers
                                   .Include(l => l.UserDetail)
                                   .ToList();
        }

        public UserInfoDto GetUserInfo(int id)
        {
            UserDetail loginUser = _schoolDBContext.Users.Find(id);
            UserInfoDto userInfo = new UserInfoDto()
            {
                Name = loginUser.Name,
                UserType = loginUser.UserType,
                EnrolledId = loginUser.EnrolledId,
                Id = loginUser.Id,
                Email = loginUser.Email,
                FullName = loginUser.FullName,
                IsEnrolled = loginUser.Enrolled_Activated,
                EmailConfirmed = loginUser.Email_Activated,
                IconURL = "http://13.236.241.62/api/Users/GetUserIcon/" + loginUser.Id
            };
            return userInfo;
        }

        public IEnumerable<UserDetail> GetAllUsers()
        {
            return _schoolDBContext.Users
                                   .Include(u => u.Student)
                                   .Include(u => u.Lecturer)
                                   .ToList();
        }

        public ReturnDto UploadIcon(int id, IFormFile file)
        {

            try
            {
                UserDetail loginUser = _schoolDBContext.Users.Find(id);

                string[] strNameArray = file.FileName.Split('.');
                string newFileName = "user" + id + "icon" + "." + strNameArray[strNameArray.Length - 1];
                //var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/images", newFileName);
                var path = Path.Combine("/home/bitnami/lms-backend/LMSbackend/LMSbackend/wwwroot/images", newFileName);
                var oldPath = loginUser.IconPath;
                if (System.IO.File.Exists(oldPath))
                {
                    System.IO.File.Delete(oldPath);
                }

                var stream = new FileStream(path, FileMode.Create);
                file.CopyToAsync(stream);

                loginUser.IconPath = path;
                loginUser.ContentType = file.ContentType;
                _schoolDBContext.SaveChanges();
                return new ReturnDto { Status = 1, Message = "Upload Successful! Refresh Page to see New Icon!" };
            }
            catch
            {
                return new ReturnDto { Status = 0, Message = "Error!" };
            }

        }

        public UserDetail GetIcon(int id)
        {
            return _schoolDBContext.Users.Find(id);
        }

        public string VerifyEmail(int id, string verifyCode)
        {
            UserDetail selectedUser = _schoolDBContext.Users.Find(id);
            if (selectedUser.EVExpiredDate < DateTime.Now)
            {
                selectedUser.Email = null;
                selectedUser.Email_Verification = null;
                _schoolDBContext.SaveChanges();
                return "Your Verification Link is Expired! Please Update Email Again! ";
            }
            else
            {
                if (selectedUser.Email_Verification == verifyCode)
                {
                    selectedUser.Email_Activated = true;
                    selectedUser.Email_Verification = null;
                    _schoolDBContext.SaveChanges();
                    return "Success, Your Email is Activated";
                }
                else
                {
                    return "Error!";
                }
            }
        }

        public string UpdateEmailAddress(int id, string email)
        {
            if (IsMailFormat(email))
            {
                UserDetail loginUser = _schoolDBContext.Users.Find(id);
                loginUser.Email = email;
                loginUser.Email_Activated = false;
                string verificationCode = MD5(VerificationNumberGenerator.Generate());
                loginUser.Email_Verification = verificationCode;
                loginUser.EVExpiredDate = DateTime.Now.AddDays(1.0);
                _schoolDBContext.SaveChanges();
                string verifyURL = string.Format("http://13.236.241.62/api/Users/VerifyEmail/{0}/{1}", id, verificationCode);
                Mailbox(email, verifyURL);
                return "Please Check Ur Email " + _schoolDBContext.Users.Find(id).Email;
            }
            else
            {
                return "please enter valid email address";
            }
        }

        /*--------------------------------USER (End)-----------------------------*/
        /*-----------------------------STUDENT-----------------------------------*/

        public IEnumerable<Student> GetAllStudents()
        {
            return _schoolDBContext.Students
                                   .Include(s => s.UserDetail)
                                   .ToList();
        }

        public IEnumerable<Student> DeleteStudent(int studentId)
        {
            Student selectedStudent = _schoolDBContext.Students.Find(studentId);
            _schoolDBContext.Students.Remove(selectedStudent);
            _schoolDBContext.SaveChanges();
            return _schoolDBContext.Students
                                   .Include(s => s.UserDetail)
                                   .ToList();
        }

        public Student AddStudent(Student student)
        {
            var studentsBefore = _schoolDBContext.Students.ToList();
            foreach (Student eachStudent in studentsBefore)
            {
                if (eachStudent.UserId == student.UserId)
                {

                    return null;
                }
            }
            student.TotalCredit = 10;
            student.IconURL = "http://13.236.241.62/api/Users/GetUserIcon/" + student.UserId;
            _schoolDBContext.Students.Add(student);
            _schoolDBContext.SaveChanges();
            var students = _schoolDBContext.Students.ToList();
            foreach (Student eachStudent in students)
            {
                if (eachStudent.UserId == student.UserId)
                {
                    _schoolDBContext.Users.Find(student.UserId).Enrolled_Activated = true;
                    _schoolDBContext.Users.Find(student.UserId).FullName = student.FullName;
                    _schoolDBContext.Users.Find(student.UserId).EnrolledId = eachStudent.Id;
                    _schoolDBContext.SaveChanges();
                    return eachStudent;
                }
            }
            return null;
        }
        /*--------------------------STUDENT (End)--------------------------------*/
        /*----------------------------LECTURER-----------------------------------*/

        public Lecturer AddLecturer(Lecturer lecturer)
        {
            var lecturersBefore = _schoolDBContext.Lecturers.ToList();
            foreach (Lecturer eachLecturer in lecturersBefore)
            {
                if (eachLecturer.UserId == lecturer.UserId)
                {

                    return null;
                }
            }
            lecturer.IconURL = "http://13.236.241.62/api/Users/GetUserIcon/" + lecturer.UserId;
            _schoolDBContext.Lecturers.Add(lecturer);
            _schoolDBContext.SaveChanges();
            var lecturers = _schoolDBContext.Lecturers.ToList();
            foreach (Lecturer eachLecturer in lecturers)
            {
                if (eachLecturer.UserId == lecturer.UserId)
                {
                    _schoolDBContext.Users.Find(lecturer.UserId).Enrolled_Activated = true;
                    _schoolDBContext.Users.Find(lecturer.UserId).FullName = lecturer.FullName;
                    _schoolDBContext.Users.Find(lecturer.UserId).EnrolledId = eachLecturer.Id;
                    _schoolDBContext.SaveChanges();
                    return eachLecturer;
                }
            }
            return null;
        }

        public IEnumerable<Lecturer> DeleteLecturer(int lecturerId)
        {
            Lecturer selectedLecturer = _schoolDBContext.Lecturers.Find(lecturerId);
            _schoolDBContext.Lecturers.Remove(selectedLecturer);
            _schoolDBContext.SaveChanges();
            return _schoolDBContext.Lecturers
                                   .Include(l => l.UserDetail)
                                   .ToList();
        }

        public IEnumerable<Lecturer> GetAllLecturers()
        {
            return _schoolDBContext.Lecturers
                                   .Include(l => l.LecturersCourses)
                                   .Include(l => l.UserDetail)
                                   .ToList();
        }

        public IEnumerable<LecturersCourses> GetAllCoursesForLecturer(int lecturerId)
        {

            return _schoolDBContext.LecturersCourses
                                   .Include(lc => lc.Course)
                                   .Where(lc => lc.LecturerId == lecturerId)
                                    .ToList();
        }

       
        /*--------------------------LECTURER (End)-------------------------------*/
        /*------------------------------COURSE-----------------------------------*/
        public Course GetCourseDetail(int courseId)
        {
            return _schoolDBContext.Courses.Find(courseId);
        }

        public IEnumerable<StudentsCourses> GetAllStudentsForCourse(int courseId)
        {

            return _schoolDBContext.StudentsCourses
                                     .Include(sc => sc.Student)
                                     .Where(sc => sc.CourseId == courseId)
                                     .ToList();
        }

        public IEnumerable<LecturersCourses> GetAllLecturersForCourse(int courseId)
        {

            return _schoolDBContext.LecturersCourses
                                   .Include(lc => lc.Lecturer)
                                   .Where(lc => lc.CourseId == courseId)
                                    .ToList();
        }

        public IEnumerable<Course> GetAllCourses()
        {
            return _schoolDBContext.Courses
                                   .Include(c => c.StudentsCourses)
                                   .Include(c => c.LecturersCourses)
                                   .ToList();
        }


        public ReturnDto DeleteCourse(int courseId)
        {
            Course selectedCourse = _schoolDBContext.Courses.Find(courseId);
            if (selectedCourse == null)
            {
                return new ReturnDto() { Status = 0, Message = "Course Not Found" };

            }
            else
            {
                _schoolDBContext.Courses.Remove(selectedCourse);
                _schoolDBContext.SaveChanges();
                return new ReturnDto() { Status = 1, Message = "Delete Successfully" };
            }

        }



        public IEnumerable<Course> AddStudentToCourse(int studentId, int courseId)
        {
            int totalCredit = _schoolDBContext.Courses.Find(courseId).Credit;
            Student selectedStudent = _schoolDBContext.Students.Find(studentId);
            var studentsCourses = _schoolDBContext.StudentsCourses.ToList();

            foreach (StudentsCourses studentcourse in studentsCourses)
            {
                if (studentcourse.StudentId == studentId)
                {
                    totalCredit += _schoolDBContext.Courses.Find(studentcourse.CourseId).Credit;
                    if (studentcourse.CourseId == courseId || totalCredit > selectedStudent.TotalCredit)
                    {
                        return null;
                    }
                }
            }



            if (selectedStudent != null)
            {
                if (_schoolDBContext.Courses.Find(courseId) != null)
                {
                    _schoolDBContext.StudentsCourses.Add(new StudentsCourses()
                    {
                        StudentId = studentId,
                        CourseId = courseId
                    });
                    _schoolDBContext.SaveChanges();
                    return _schoolDBContext.Courses
                                   .Include(c => c.StudentsCourses)
                                   .ToList();
                }
            }
            return null;
        }

        public IEnumerable<Course> DeleteStudentFromCourse(int studentId, int courseId)
        {
            var studentsCourses = _schoolDBContext.StudentsCourses.ToList();
            foreach (StudentsCourses studentcourse in studentsCourses)
            {
                if (studentcourse.StudentId == studentId)
                {
                    if (studentcourse.CourseId == courseId)
                    {
                        _schoolDBContext.StudentsCourses.Remove(studentcourse);
                        _schoolDBContext.SaveChanges();
                        return _schoolDBContext.Courses
                                   .Include(c => c.StudentsCourses)
                                   .ToList();
                    }
                }
            }
            return null;
        }

        public IEnumerable<Course> AddLecturerToCourse(int lecturerId, int courseId)
        {
            var lecturersCourses = _schoolDBContext.LecturersCourses.ToList();
            foreach (LecturersCourses lecturerCourse in lecturersCourses)
            {
                if (lecturerCourse.LecturerId == lecturerId)
                {
                    if (lecturerCourse.CourseId == courseId)
                    {
                        return null;
                    }
                }
            }

            Lecturer selectedLecturer = _schoolDBContext.Lecturers.Find(lecturerId);
            if (selectedLecturer != null)
            {
                if (_schoolDBContext.Courses.Find(courseId) != null)
                {
                    _schoolDBContext.LecturersCourses.Add(new LecturersCourses()
                    {
                        LecturerId = lecturerId,
                        CourseId = courseId
                    });
                    _schoolDBContext.SaveChanges();
                    return _schoolDBContext.Courses
                                           .Include(c => c.LecturersCourses)
                                            .ToList();
                }
            }
            return null;
        }

        public IEnumerable<Course> DeleteLecturerFromCourse(int lecturerId, int courseId)
        {
            var lecturersCourses = _schoolDBContext.LecturersCourses.ToList();
            foreach (LecturersCourses lecturerCourse in lecturersCourses)
            {
                if (lecturerCourse.LecturerId == lecturerId)
                {
                    if (lecturerCourse.CourseId == courseId)
                    {
                        _schoolDBContext.LecturersCourses.Remove(lecturerCourse);
                        _schoolDBContext.SaveChanges();
                        return _schoolDBContext.Courses
                                               .Include(c => c.LecturersCourses)
                                               .ToList();
                    }
                }
            }
            return null;
        }

        public ReturnDto AddCourse(Course course)
        {
            var coursesBefore = _schoolDBContext.Courses.ToList();
            foreach (Course eachCourse in coursesBefore)
            {
                if (eachCourse.CourseName == course.CourseName)
                {

                    return new ReturnDto { Status = 0, Message = "Course Name Already Exist" };
                }
            }
            int? credit = course.Credit;
            if (!credit.HasValue)
            {
                course.Credit = 0;
            }
            else if (credit > 3 || credit < 0)
            {
                return new ReturnDto { Status = 0, Message = "Credit Only Avialable Between 0-3 " };
            }
            if (course.Description == "")
            {
                course.Description = "This Course Do Not Have a Description yet";
            }
            _schoolDBContext.Courses.Add(course);
            _schoolDBContext.SaveChanges();
            return new ReturnDto { Status = 1, Message = "Add Successfully" };
        }

        public ReturnDto UpdateCourse(Course course, int courseId)
        {
            Course selectedCourse = _schoolDBContext.Courses.Find(courseId);
            selectedCourse.Credit = course.Credit;
            selectedCourse.Description = course.Description;
            _schoolDBContext.SaveChanges();
            return new ReturnDto { Status = 1, Message = "Update Successful !" };
        }


        /*--------------------------COURSE (End)---------------------------------*/

        private static string MD5(string code)
        {

            string MD5code = "";

            byte[] buffer = Encoding.UTF8.GetBytes(code);   //convert to Byte[]    

            byte[] MD5buffer = new MD5CryptoServiceProvider().ComputeHash(buffer); //instantiation and encryption

            foreach (byte item in MD5buffer)
            //convert each byte[] to string, from duotricemary（32） to Hexadecimal（16)
            {
                MD5code += item.ToString("X2");
            }
            return MD5code;
        }

        private static bool Mailbox(string clientEmail, string verifyURL)
        {

            MailMessage mail = new MailMessage();
            mail.From = new MailAddress("tuscmedia.au@gmail.com");

            // The important part -- configuring the SMTP client
            SmtpClient smtp = new SmtpClient();
            smtp.Port = 587;   // You can try with 465 also
            smtp.EnableSsl = true;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential("tuscmedia.au@gmail.com", "Ma950522");  // account,psw
            smtp.Host = "smtp.gmail.com";

            //recipient address
            mail.To.Add(new MailAddress(clientEmail));
            mail.Subject = "mailbox verification";
            //Formatted mail body
            mail.IsBodyHtml = true;

            mail.Body = "click this link to verify ur Email : " + verifyURL;
            smtp.Send(mail);
            return true;
        }

        //checking email format
        private static bool IsMailFormat(string mailFormat)
        {
            for (int i = 0; i < mailFormat.Length; ++i)
            {
                if (';' == mailFormat[i])
                {
                    return false;
                }
            }

            return Regex.IsMatch(mailFormat, @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
        }



        private string GetSalt()
        {
            // random salt 
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] saltBytes = new byte[36];
            rng.GetNonZeroBytes(saltBytes);
            string salt = Convert.ToBase64String(saltBytes);
            return salt;
        }

        private string PSWEncryption(string psw, string salt)

        {

            byte[] passwordAndSaltBytes = Encoding.UTF8.GetBytes(psw + salt);
            byte[] hashBytes = new SHA256Managed().ComputeHash(passwordAndSaltBytes);

            string hashString = Convert.ToBase64String(hashBytes);
            return hashString;
        }

        private static bool SendMessage(string phone, string verificationNumber, DateTime expiredDateTime)
        {
            try
            {
                var webClient = new WebClient();
                webClient.UseDefaultCredentials = true;
                webClient.Encoding = Encoding.UTF8;
                webClient.Headers.Add(HttpRequestHeader.ContentType, "application/x-www-form-urlencoded");

                var reqparm = new NameValueCollection();
                reqparm.Add("client_id", "fdzoplaonrDArfYkhS85JVAPnp01wRHJ");
                reqparm.Add("client_secret", "oJIjum7oNWC5E6QC");
                reqparm.Add("grant_type", "client_credentials");

                Byte[] tokenResponse = webClient.UploadValues("https://sapi.telstra.com/v1/oauth/token", reqparm);
                string result = Encoding.UTF8.GetString(tokenResponse);
                var obj = JObject.Parse(result);
                string token = obj.GetValue("access_token").ToString();


                webClient.Headers.Clear();
                webClient.Encoding = Encoding.UTF8;
                webClient.Headers.Add(HttpRequestHeader.Accept, "application/json");
                webClient.Headers.Add(HttpRequestHeader.ContentType, "application/json");
                webClient.Headers.Add(HttpRequestHeader.Authorization, "Bearer " + token);
                webClient.UploadString("https://tapi.telstra.com/v2/messages/provisioning/subscriptions", "{}");

                webClient.Headers.Clear();
                webClient.UseDefaultCredentials = true;
                webClient.Encoding = Encoding.UTF8;
                webClient.Headers.Add(HttpRequestHeader.Accept, "application/json");
                webClient.Headers.Add(HttpRequestHeader.ContentType, "application/json");
                webClient.Headers.Add(HttpRequestHeader.Authorization, "Bearer " + token);

                PostSMSDto postData = new PostSMSDto()
                {
                    to = phone,
                    body = "Your Verification Number is " + verificationNumber + "\r\n Expired By " + expiredDateTime.ToString()
                };

                string requestJson = JsonConvert.SerializeObject(postData);
                webClient.UploadString("https://tapi.telstra.com/v2/messages/sms", requestJson);
                return true;
            }
            catch
            {
                return false;
            }
        }

    }
}
