﻿using System;
using System.Collections.Generic;

namespace LMSbackend.Model
{
    public class Course
    {
        public int Id { get; set; }
        public string CourseName { get; set; }
        public int Credit { get; set; }
        public string Description { get; set; }


        public ICollection<LecturersCourses> LecturersCourses { get; set; }
        public ICollection<StudentsCourses> StudentsCourses { get; set; }

    }
}
