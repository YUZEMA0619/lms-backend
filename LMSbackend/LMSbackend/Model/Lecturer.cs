﻿using System;
using System.Collections.Generic;

namespace LMSbackend.Model
{
    public class Lecturer
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string IconURL { get; set; }
        public int UserId { get; set; }


        public UserDetail UserDetail { get; set; }
        public ICollection<LecturersCourses> LecturersCourses { get; set; }
    }
}
