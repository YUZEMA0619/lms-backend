﻿using System;
namespace LMSbackend.Model
{
    public class UserDetail
    {

        public int Id { get; set; }
        public string Name { get; set; }
        public string UserType { get; set; }
        public string Account { get; set; }

        public string Password { get; set; }
        public string Salt { get; set; }

        //One to One relationship status with student or lecturer
        public int EnrolledId { get; set; }
        public bool Enrolled_Activated { get; set; }

        public string FullName { get; set; }

        public string Email { get; set; }
        public bool Email_Activated { get; set; }
        public string Email_Verification { get; set; }
        public DateTime EVExpiredDate { get; set; }

        public string Phone { get; set; }
        public bool Phone_Activated { get; set; }
        public string VerificationNumber { get; set; }
        public DateTime VNExpiredDate { get; set; }

        public string IconPath { get; set; }
        public string ContentType { get; set; }

        public Student Student { get; set; }
        public Lecturer Lecturer { get; set; }

    }
}
