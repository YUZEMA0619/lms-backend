﻿using System;
using System.Collections.Generic;

namespace LMSbackend.Model
{
    public class Student
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string IconURL { get; set; }
        public int TotalCredit { get; set; }
        public int UserId { get; set; }


        public UserDetail UserDetail { get; set; }
        public ICollection<StudentsCourses> StudentsCourses { get; set; }
        //public string FirstName { get; set; }
        //public string LastName { get; set; }
        //public int Age { get; set; }
        //public string Gender { get; set; }
    }
}
