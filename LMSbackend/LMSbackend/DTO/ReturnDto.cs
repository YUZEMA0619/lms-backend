﻿using System;
namespace LMSbackend.DTO
{
    public class ReturnDto
    {
        public int Status { get; set; }
        public string Message { get; set; }
    }
}
