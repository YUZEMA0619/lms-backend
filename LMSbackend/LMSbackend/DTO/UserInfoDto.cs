﻿using System;
namespace LMSbackend.DTO
{
    public class UserInfoDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string UserType { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public bool IsEnrolled { get; set; }
        public bool EmailConfirmed { get; set; }
        public string IconURL { get; set; }
        //EnrolledId refered to studentId or lectureId
        public int EnrolledId { get; set; }
    }
}
