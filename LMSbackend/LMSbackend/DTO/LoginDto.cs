﻿using System;
namespace LMSbackend.DTO
{
    public class LoginDto
    {
        public string Account { get; set; }
        public string Password { get; set; }
        public string VerificationNumber { get; set; }
    }
}
