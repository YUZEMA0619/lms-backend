﻿using System;
using System.Collections.Generic;
using LMSbackend.DTO;
using LMSbackend.Model;
using Microsoft.AspNetCore.Http;

namespace LMSbackend.Interface
{
    public interface IUserDataStore
    {
        ReturnDto LogIn(string account, string password, string verificationNumber);
        ReturnDto SignUp(UserDetail userDetail);

        UserInfoDto GetUserInfo(int id);
        ReturnDto UploadIcon(int id, IFormFile file);
        UserDetail GetIcon(int id);

        IEnumerable<Course> DeleteStudentFromCourse(int studentId, int courseId);
        IEnumerable<Course> AddStudentToCourse(int studentId, int courseId);
        IEnumerable<Course> DeleteLecturerFromCourse(int lecturerId, int courseId);
        IEnumerable<Course> AddLecturerToCourse(int lecturerId, int courseId);

        Student AddStudent(Student student);
        Lecturer AddLecturer(Lecturer lecturer);
        ReturnDto AddCourse(Course course);
        ReturnDto UpdateCourse(Course course, int courseId);


        IEnumerable<Student> GetAllStudents();
        IEnumerable<Student> DeleteStudent(int studentId);
        IEnumerable<Student> DeleteStudentUser(int userId);

        IEnumerable<Lecturer> GetAllLecturers();
        IEnumerable<Lecturer> DeleteLecturer(int lecturerId);
        IEnumerable<Lecturer> DeleteLecturerUser(int userId);

        IEnumerable<Course> GetAllCourses();
        IEnumerable<UserDetail> GetAllUsers();

        Course GetCourseDetail(int courseId);
        ReturnDto DeleteCourse(int courseId);

        IEnumerable<StudentsCourses> GetAllStudentsForCourse(int courseId);
        IEnumerable<LecturersCourses> GetAllLecturersForCourse(int courseId);
        IEnumerable<LecturersCourses> GetAllCoursesForLecturer(int lecturerId);
        string UpdateEmailAddress(int id, string email);
        string VerifyEmail(int id, string verifyCode);
    }
}
